import React, {useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import Grid from "./Grid";
import {TestSet} from "./types";

import './EditPanel.css';

function generatePattern(level: number): number[] {
    const pattern = Array.from({length: 2 * level}, (_, index) => index);
    for (let i = 0; i < level; i++) {
        pattern.splice(0, 0, ...pattern.splice(Math.floor(Math.random() * (pattern.length - i)) + i, 1));
    }
    return pattern.slice(0, level).sort((a, b) => a - b);
}

type EditPanelProps = {
    testSets: TestSet[];
    onSave: (tests: TestSet[]) => void;
}

function EditPanel({testSets: initialTestSets, onSave}: EditPanelProps) {
    const {t} = useTranslation();

    const [testSets, setTestSets] = useState(initialTestSets);

    const handleCellClick = (testSetIndex: number, testCaseIndex: number, cellNo: number) => {
        setTestSets(tests => [
            ...tests.slice(0, testSetIndex),
            tests[testSetIndex].map((testCase, thisCaseIndex) => {
                if (testCaseIndex !== thisCaseIndex) return testCase;

                const index = testCase.highlighted.indexOf(cellNo);
                if (index === -1) {
                    return {
                        ...testCase,
                        highlighted: [...testCase.highlighted, cellNo].sort((a, b) => a - b),
                    }
                } else {
                    return {
                        ...testCase,
                        highlighted: [...testCase.highlighted.slice(0, index), ...testCase.highlighted.slice(index + 1)]
                    }
                }
            }),
            ...tests.slice(testSetIndex + 1),
        ]);
    }

    const handleRandomize = (testSetIndex: number, testCaseIndex: number) => {
        setTestSets(tests => [
            ...tests.slice(0, testSetIndex),
            tests[testSetIndex].map((testCase, thisCaseIndex) => {
                return testCaseIndex !== thisCaseIndex
                    ? testCase
                    : {...testCase, highlighted: generatePattern(testCase.difficulty)};
            }),
            ...tests.slice(testSetIndex + 1),
        ]);
    }

    const isValid = useMemo(() => testSets.every(
        testSet => testSet.every(
            testCase =>
                testCase.difficulty === testCase.highlighted.length
        )
    ), [testSets]);

    const handleSave = () => {
        if (isValid) {
            onSave(testSets);
        }
    }

    return (
        <div className="EditPanel">
            <form>
                <button type="button" onClick={handleSave} disabled={!isValid}>{t("editor.save")}</button>
            </form>
            <div className="TestSets">
                {testSets.map((testSet, testSetIndex) => (
                    <div key={testSetIndex} className="TestSet">
                        {testSet.map(({difficulty, columns, highlighted}, testIndex) => (
                            <div key={testIndex}
                                 className={"TestCase " + (difficulty === highlighted.length ? "correct" : "error")}>
                                <Grid cells={2 * difficulty} cols={columns} highlighted={highlighted}
                                      onCellClick={(cellNo) => handleCellClick(testSetIndex, testIndex, cellNo)}
                                      size="mini"/>
                                <button type="button" onClick={() => handleRandomize(testSetIndex, testIndex)}>
                                    {t("editor.random")}
                                </button>
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        </div>
    );
}

export default EditPanel;
