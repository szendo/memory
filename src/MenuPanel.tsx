import React from 'react';
import {useTranslation} from "react-i18next";

import './MenuPanel.css';

type MenuPanelProps = {
    onStart: () => void;
    onResults: () => void;
    onEdit: () => void;
    onLoad: () => void;
};

function MenuPanel({onEdit, onResults, onLoad, onStart}: MenuPanelProps) {
    const {t, i18n} = useTranslation();

    return (
        <form className="MenuPanel">
            <h1>{t("title")}</h1>
            <div className="LangSelector">
                <button type="button" onClick={() => void i18n.changeLanguage("hu")}>🇭🇺</button>
                <button type="button" onClick={() => void i18n.changeLanguage("en")}>🇬🇧</button>
            </div>
            <button onClick={onStart} type="button">{t("menu.start")}</button>
            <button onClick={onResults} type="button">{t("menu.results")}</button>
            <button onClick={onEdit} type="button">{t("menu.edit")}</button>
            <button onClick={onLoad} type="button">{t("menu.load")}</button>
            <div className="Bibliography">
                <div className="Title">{t("menu.bibliography")}</div>
                Della Sala, S. ; Gray, C.; Baddeley, A.; Allamano, N. & Wilson, L. (1999). Pattern span: a tool for
                unwelding visuo–spatial memory. , <em>Neuropsychologia 37</em>(10), 1189–1199.<br/>
                DOI:10.1016/s0028-3932(98)00159-6<br/>
                <br/>
                Kovács G. (2011-2014) MAMUT magyar munkamemória-teszt csomag
            </div>
            <div className="Credits">
                <div className="Title">{t("menu.credits")} Jenes Tünde, Szendrei Péter</div>
            </div>
        </form>
    );
}

export default MenuPanel;
