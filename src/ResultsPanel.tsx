import React, {useEffect, useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import {testSets} from "./testSets";
import {Results} from "./types";

import "./ResultsPanel.css"

type ResultsPanelProps = {
    results: Results;
    onBack: () => void;
};

function ResultsPanel({results, onBack}: ResultsPanelProps) {
    const {t} = useTranslation();

    const headerRow = useMemo(() => [
        t("stats.subjectId"),
        ...testSets.flatMap(testSet => testSet.map((testCase, caseIndex) => `${testCase.difficulty}/${caseIndex + 1}`)),
    ], [t]);

    const resultRows = useMemo(() => results.map(({subjectId, results}) => [
        subjectId.toString(),
        ...testSets.flatMap((testSet, setIndex) =>
            testSet.map((_, caseIndex) => results[setIndex * 3 + caseIndex]?.score?.toString())),
    ]), [results]);

    const [csvUrl, setCsvUrl] = useState("");

    useEffect(() => {
        const csvSep = ","
        const csvData = [headerRow.join(csvSep), ...resultRows.map(row => row.join(csvSep))].join("\r\n");
        const csvUrl = URL.createObjectURL(new Blob([csvData], {type: "text/csv"}));
        setCsvUrl(csvUrl);
        return () => URL.revokeObjectURL(csvUrl);
    }, [headerRow, resultRows]);

    return (
        <div className="ResultsPanel">
            <form>
                <button type="button" onClick={onBack}>{t("basic.back")}</button>
            </form>
            <table>
                <thead>
                <tr>
                    {headerRow.map((value, index) => <th key={index}>{value}</th>)}
                </tr>
                </thead>
                <tbody>
                {resultRows.map((row, rowIndex) => (
                    <tr key={rowIndex}>
                        {row.map((value, index) => <td key={index}>{value}</td>)}
                    </tr>
                ))}
                </tbody>
            </table>
            <a href={csvUrl} download="vpt-results.csv">{t("stats.download")}</a>
        </div>
    );
}

export default ResultsPanel;
