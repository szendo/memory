import i18next from "i18next";
import React from 'react';
import {createRoot} from "react-dom/client";
import {initReactI18next} from "react-i18next";
import App from "./App";
import {en, hu} from "./lang";

import './index.css';

void i18next
    .use(initReactI18next)
    .init({
        resources: {
            en: {translation: en},
            hu: {translation: hu},
        },
        lng: "hu",
        fallbackLng: "hu",
        interpolation: {
            escapeValue: false,
        },
    });

createRoot(document.getElementById("root")!)
    .render(
        <React.StrictMode>
            <App/>
        </React.StrictMode>
    );
