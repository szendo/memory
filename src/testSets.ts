import {TestSet} from "./types";

export const testSets: TestSet[] = [
    [
        {
            "difficulty": 2,
            "columns": 2,
            "highlighted": [1, 2],
        },
        {
            "difficulty": 2,
            "columns": 2,
            "highlighted": [0, 3],
        },
        {
            "difficulty": 2,
            "columns": 2,
            "highlighted": [0, 2],
        },
    ],
    [
        {
            "difficulty": 3,
            "columns": 3,
            "highlighted": [0, 3, 5],
        },
        {
            "difficulty": 3,
            "columns": 3,
            "highlighted": [1, 2, 3],
        },
        {
            "difficulty": 3,
            "columns": 3,
            "highlighted": [1, 3, 5],
        },
    ],
    [
        {
            "difficulty": 4,
            "columns": 3,
            "highlighted": [0, 2, 5, 6],
        },
        {
            "difficulty": 4,
            "columns": 3,
            "highlighted": [0, 3, 5, 7],
        },
        {
            "difficulty": 4,
            "columns": 3,
            "highlighted": [1, 2, 4, 6],
        },
    ],
    [
        {
            "difficulty": 5,
            "columns": 4,
            "highlighted": [0, 2, 5, 6, 8],
        },
        {
            "difficulty": 5,
            "columns": 4,
            "highlighted": [0, 4, 6, 7, 9],
        },
        {
            "difficulty": 5,
            "columns": 4,
            "highlighted": [0, 3, 6, 7, 9],
        },
    ],
    [
        {
            "difficulty": 6,
            "columns": 4,
            "highlighted": [1, 3, 5, 6, 8, 11],
        },
        {
            "difficulty": 6,
            "columns": 4,
            "highlighted": [0, 1, 4, 7, 8, 10],
        },
        {
            "difficulty": 6,
            "columns": 4,
            "highlighted": [1, 2, 4, 7, 9, 11],
        },
    ],
    [
        {
            "difficulty": 7,
            "columns": 4,
            "highlighted": [0, 1, 3, 6, 8, 12, 13],
        },
        {
            "difficulty": 7,
            "columns": 4,
            "highlighted": [1, 3, 4, 5, 6, 10, 13],
        },
        {
            "difficulty": 7,
            "columns": 4,
            "highlighted": [1, 4, 7, 8, 10, 11, 13],
        },
    ],
    [
        {
            "difficulty": 8,
            "columns": 4,
            "highlighted": [0, 3, 4, 6, 9, 12, 13, 14],
        },
        {
            "difficulty": 8,
            "columns": 4,
            "highlighted": [0, 1, 3, 4, 9, 11, 12, 15],
        },
        {
            "difficulty": 8,
            "columns": 4,
            "highlighted": [0, 3, 5, 7, 8, 10, 14, 15],
        },
    ],
    [
        {
            "difficulty": 9,
            "columns": 5,
            "highlighted": [0, 2, 3, 9, 10, 11, 13, 15, 17],
        },
        {
            "difficulty": 9,
            "columns": 5,
            "highlighted": [0, 1, 3, 6, 7, 9, 10, 14, 16],
        },
        {
            "difficulty": 9,
            "columns": 5,
            "highlighted": [0, 1, 3, 4, 7, 9, 10, 15, 17],
        },
    ],
    [
        {
            "difficulty": 10,
            "columns": 5,
            "highlighted": [0, 4, 5, 7, 8, 11, 13, 14, 15, 18],
        },
        {
            "difficulty": 10,
            "columns": 5,
            "highlighted": [1, 4, 5, 7, 9, 12, 13, 16, 17, 19],
        },
        {
            "difficulty": 10,
            "columns": 5,
            "highlighted": [0, 2, 3, 5, 9, 11, 13, 15, 18, 19],
        },
    ],
    [
        {
            "difficulty": 11,
            "columns": 5,
            "highlighted": [0, 3, 4, 5, 6, 14, 15, 17, 18, 19, 21],
        },
        {
            "difficulty": 11,
            "columns": 5,
            "highlighted": [1, 2, 4, 5, 7, 9, 12, 14, 18, 20, 21],
        },
        {
            "difficulty": 11,
            "columns": 5,
            "highlighted": [0, 3, 4, 5, 7, 8, 11, 13, 16, 19, 20],
        },
    ],
    [
        {
            "difficulty": 12,
            "columns": 5,
            "highlighted": [0, 1, 4, 6, 9, 11, 12, 14, 15, 18, 20, 22],
        },
        {
            "difficulty": 12,
            "columns": 5,
            "highlighted": [0, 3, 4, 5, 11, 13, 14, 15, 18, 20, 21, 23],
        },
        {
            "difficulty": 12,
            "columns": 5,
            "highlighted": [0, 1, 5, 7, 9, 14, 15, 17, 19, 21, 22, 23],
        },
    ],
    [
        {
            "difficulty": 13,
            "columns": 6,
            "highlighted": [1, 3, 6, 11, 12, 13, 14, 17, 20, 22, 23, 24, 25],
        },
        {
            "difficulty": 13,
            "columns": 6,
            "highlighted": [0, 2, 3, 4, 9, 10, 11, 12, 17, 19, 21, 22, 24],
        },
        {
            "difficulty": 13,
            "columns": 6,
            "highlighted": [2, 3, 5, 7, 10, 12, 13, 15, 18, 21, 22, 23, 24],
        },
    ],
    [
        {
            "difficulty": 14,
            "columns": 6,
            "highlighted": [0, 2, 4, 5, 6, 9, 14, 15, 17, 19, 20, 21, 23, 27],
        },
        {
            "difficulty": 14,
            "columns": 6,
            "highlighted": [0, 2, 5, 9, 10, 11, 12, 15, 19, 21, 22, 24, 25, 26],
        },
        {
            "difficulty": 14,
            "columns": 6,
            "highlighted": [0, 3, 5, 7, 8, 9, 11, 15, 16, 18, 20, 21, 24, 25],
        },
    ],
    [
        {
            "difficulty": 15,
            "columns": 6,
            "highlighted": [0, 1, 2, 5, 11, 12, 13, 15, 17, 18, 22, 23, 25, 27, 28],
        },
        {
            "difficulty": 15,
            "columns": 6,
            "highlighted": [1, 4, 6, 9, 10, 12, 13, 15, 16, 17, 21, 25, 27, 28, 29],
        },
        {
            "difficulty": 15,
            "columns": 6,
            "highlighted": [0, 3, 5, 6, 10, 12, 13, 15, 16, 17, 23, 25, 26, 27, 29],
        },
    ],
];
