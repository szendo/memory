export type TestCase = {
    difficulty: number;
    columns: number;
    highlighted: number[];
};

export type TestSet = TestCase[];

export type StatLine = {
    testCase: TestCase;
    caseNo: number;
    recallPattern: number[];
    score: number;
};

export type Results = {
    subjectId: number;
    results: StatLine[];
}[];
